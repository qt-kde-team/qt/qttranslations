qttranslations-opensource-src (5.15.15-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 25 Oct 2024 12:48:40 +0300

qttranslations-opensource-src (5.15.15-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.15.
  * Bump Standards-Version to 4.7.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 08 Sep 2024 12:28:43 +0300

qttranslations-opensource-src (5.15.13-2) unstable; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Remove Timo and myself from Uploaders.

  [ Dmitry Shachnev ]
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 21 May 2024 18:52:58 +0300

qttranslations-opensource-src (5.15.13-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.13.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 12 Mar 2024 17:55:52 +0300

qttranslations-opensource-src (5.15.12-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.12.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 23 Dec 2023 19:17:36 +0300

qttranslations-opensource-src (5.15.10-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 08 Jul 2023 19:18:31 +0300

qttranslations-opensource-src (5.15.10-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.10.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 11 Jun 2023 00:20:09 +0300

qttranslations-opensource-src (5.15.9-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.9.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 23 Apr 2023 00:13:28 +0300

qttranslations-opensource-src (5.15.8-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 13 Jan 2023 12:02:16 +0400

qttranslations-opensource-src (5.15.8-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.8.
  * Bump Standards-Version to 4.6.2, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 07 Jan 2023 17:04:59 +0400

qttranslations-opensource-src (5.15.7-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 17 Dec 2022 18:20:37 +0300

qttranslations-opensource-src (5.15.7-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.7.

 -- Dmitry Shachnev <mitya57@debian.org>  Sun, 04 Dec 2022 20:47:29 +0300

qttranslations-opensource-src (5.15.6-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 29 Sep 2022 11:55:49 +0300

qttranslations-opensource-src (5.15.6-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.6.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 13 Sep 2022 13:54:25 +0300

qttranslations-opensource-src (5.15.5-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.5.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 30 Jul 2022 21:11:01 +0300

qttranslations-opensource-src (5.15.4-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 13 Jun 2022 21:36:58 +0300

qttranslations-opensource-src (5.15.4-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.4.
  * Bump Standards-Version to 4.6.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Sat, 14 May 2022 12:57:14 +0300

qttranslations-opensource-src (5.15.3-1) experimental; urgency=medium

  * New upstream release.
  * Update debian/watch.
  * Bump Qt build-dependencies to 5.15.3.
  * Update email address for Patrick Franz.
  * Bump Standards-Version to 4.6.0, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 08 Mar 2022 22:49:18 +0300

qttranslations-opensource-src (5.15.2-2) unstable; urgency=medium

  * Update debian/watch: use format 4, and track only 5.15.x releases.
  * Bump Standards-Version to 4.5.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Tue, 26 Jan 2021 16:33:26 +0300

qttranslations-opensource-src (5.15.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.15.2.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 23 Nov 2020 21:28:30 +0300

qttranslations-opensource-src (5.15.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 28 Oct 2020 21:54:17 +0300

qttranslations-opensource-src (5.15.1-1) experimental; urgency=medium

  [ Patrick Franz ]
  * New upstream release.
  * Bump Qt build-dependencies to 5.15.1.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Tue, 15 Sep 2020 23:19:04 -0300

qttranslations-opensource-src (5.14.2-2) unstable; urgency=medium

  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 24 Jun 2020 12:31:28 +0300

qttranslations-opensource-src (5.14.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.5.0, no changes needed.
  * Bump debhelper-compatibility to 13:
    - Switch debhelper build dependency to debhelper-compat 13.
    - Remove debian/compat.
  * Update the Uploaders-field.
  * Bump Qt build-dependencies to 5.14.2.
  * Add Rules-Requires-Root field.
  * Update debian/copyright.

 -- Patrick Franz <patfra71@gmail.com>  Wed, 13 May 2020 19:22:29 +0200

qttranslations-opensource-src (5.12.5-1) unstable; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.12.5.
  * Bump Standards-Version to 4.4.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 21 Oct 2019 23:25:35 +0300

qttranslations-opensource-src (5.12.4-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.12.4.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 28 Jun 2019 21:32:28 +0300

qttranslations-opensource-src (5.12.2-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.12.2.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 27 Mar 2019 21:55:25 +0300

qttranslations-opensource-src (5.11.3-2) unstable; urgency=medium

  [ Simon Quigley ]
  * Change my email to tsimonq2@debian.org now that I am a Debian Developer.
  * Bump Standards-version to 4.3.0, no changes needed.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 26 Dec 2018 16:56:38 -0300

qttranslations-opensource-src (5.11.3-1) experimental; urgency=medium

  * New upstream release.
    - Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 22 Dec 2018 15:21:18 -0300

qttranslations-opensource-src (5.11.2-2) unstable; urgency=medium

  * Bump qttools5-dev-tools build-dependency to 5.11.2.
  * Bump Standards-Version to 4.2.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 17 Oct 2018 10:35:23 +0300

qttranslations-opensource-src (5.11.2-1) experimental; urgency=medium

  * New upstream release.
    - Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Mon, 08 Oct 2018 16:15:55 -0300

qttranslations-opensource-src (5.11.1-2) unstable; urgency=medium

  * Upload to Sid.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Wed, 25 Jul 2018 04:49:32 -0500

qttranslations-opensource-src (5.11.1-1) experimental; urgency=medium

  * New upstream release.
  * Bump Standards-version to 4.1.4, no changes needed.
  * Bump build dependencies to 5.11.1.
  * Fix insecure-copyright-format-uri.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Sat, 30 Jun 2018 17:23:40 -0500

qttranslations-opensource-src (5.10.1-2) unstable; urgency=medium

  * Release to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 07 Apr 2018 19:40:22 -0300

qttranslations-opensource-src (5.10.1-1) experimental; urgency=medium

  * New upstream release.
    - Bump Qt build dependencies.
  * Switch Vcs-[Git Browser] to salsa.debian.org.
  * Switch to debhelper compat 11.
  * Update Standards-Version to 4.1.3, no changes required.
  * Update debian/watch to follow upstream's naming conventions.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 14 Mar 2018 10:59:27 -0300

qttranslations-opensource-src (5.9.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.9.2.
  * Update to debhelper compat level 10.
  * Bump Standards-Version to 4.1.1, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 27 Oct 2017 21:55:13 +0300

qttranslations-opensource-src (5.9.1-1) unstable; urgency=medium

  * New upstream release.
  * Update debian/copyright for upstream license change.
  * Bump Standards-Version to 4.0.1, no changes needed.
  * Upload to unstable.

 -- Dmitry Shachnev <mitya57@debian.org>  Wed, 16 Aug 2017 15:13:44 +0300

qttranslations-opensource-src (5.9.0-1) experimental; urgency=medium

  * New upstream release.
  * Add myself to Uploaders.

 -- Simon Quigley <tsimonq2@ubuntu.com>  Thu, 15 Jun 2017 08:47:17 -0500

qttranslations-opensource-src (5.7.1~20161021-1) unstable; urgency=medium

  * New upstream snapshot.
    - Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 26 Oct 2016 18:36:43 -0300

qttranslations-opensource-src (5.6.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Mon, 13 Jun 2016 11:25:17 -0300

qttranslations-opensource-src (5.6.1-1) experimental; urgency=medium

  * New upstream release.
    - Bump Qt build dependencies.
  * Bump Standards-Version to 3.9.8, no changes required.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sun, 12 Jun 2016 18:18:26 -0300

qttranslations-opensource-src (5.6.0-1) experimental; urgency=medium

  * New upstream release. Bump build dependencies.
  * Bump Standards-Version to 3.9.7, no changes.
  * Switch vcs URLs to https.

 -- Timo Jyrinki <timo@debian.org>  Wed, 06 Apr 2016 12:18:14 +0300

qttranslations-opensource-src (5.5.1-2) unstable; urgency=medium

  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 21 Oct 2015 22:01:16 -0300

qttranslations-opensource-src (5.5.1-1) experimental; urgency=medium

  * New upstream release.
    - Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Mon, 19 Oct 2015 13:18:46 -0300

qttranslations-opensource-src (5.5.0-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.5.0.
  * Update debian/copyright.

 -- Dmitry Shachnev <mitya57@debian.org>  Fri, 28 Aug 2015 14:57:16 +0300

qttranslations-opensource-src (5.4.2-2) unstable; urgency=medium

  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 24 Jun 2015 17:50:04 -0300

qttranslations-opensource-src (5.4.2-1) experimental; urgency=medium

  [ Timo Jyrinki ]
  * Update debian/copyright.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * New upstream release.
    - Bump Qt build dependencies.
  * Clear up the list in Uploaders removing people who hasn't committed to the
    repo in more than a year. They can re add themselves whenever they want
    (and we really hope to see you back really soon!).

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 03 Jun 2015 16:50:19 -0300

qttranslations-opensource-src (5.4.1-1) experimental; urgency=medium

  * New upstream release.
  * Update watch file to qt.io.
  * Bump Qt build-dependencies to 5.4.1.

 -- Timo Jyrinki <timo@debian.org>  Wed, 04 Mar 2015 12:04:03 +0000

qttranslations-opensource-src (5.4.0-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build-dependencies to 5.4.0.
  * Update debian/copyright.
  * Bump Standards-Version to 3.9.6, no changes needed.

 -- Dmitry Shachnev <mitya57@debian.org>  Mon, 22 Dec 2014 14:32:18 +0300

qttranslations-opensource-src (5.3.2-2) unstable; urgency=medium

  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Tue, 23 Sep 2014 00:06:01 -0300

qttranslations-opensource-src (5.3.2-1) experimental; urgency=medium

  * New upstream release.
  * Update my e-mail address.
  * Update Vcs-Browser field to point to cgit interface.
  * Update build-dependencies for Qt 5.3.2.
  * Update debian/copyright.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 18 Sep 2014 13:11:02 +0400

qttranslations-opensource-src (5.3.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Mon, 30 Jun 2014 18:14:39 -0300

qttranslations-opensource-src (5.3.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Fri, 06 Jun 2014 16:15:03 -0300

qttranslations-opensource-src (5.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build dependencies.
  * Update debian/copyright.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 28 May 2014 23:41:25 -0300

qttranslations-opensource-src (5.2.1-2) unstable; urgency=medium

  [ Dmitry Shachnev ]
  * Remove dh_builddeb override, xz is now the default compression method.

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Tue, 25 Mar 2014 00:07:12 -0300

qttranslations-opensource-src (5.2.1-1) experimental; urgency=medium

  * New upstream release.
  * Bump Qt build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Wed, 19 Feb 2014 19:48:06 -0300

qttranslations-opensource-src (5.2.0-2) unstable; urgency=medium

  * Upload to unstable.
  * Fix orthography in previous changelog.
  * Update Standards-Version to 3.9.5, no changes required.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Fri, 31 Jan 2014 17:38:12 -0300

qttranslations-opensource-src (5.2.0-1) experimental; urgency=low

  [ Dmitry Shachnev ]
  * New upstream release.
  * Bump qtbase and qttools build-dependencies to 5.2.0.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Fri, 13 Dec 2013 14:31:25 -0300

qttranslations-opensource-src (5.1.1-1) unstable; urgency=low

  * New upstream release.
  * Tighten Qt 5 build dpendencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Fri, 30 Aug 2013 18:19:40 -0300

qttranslations-opensource-src (5.1.0-2) unstable; urgency=low

  * Upload to unstable.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sun, 11 Aug 2013 13:31:31 -0300

qttranslations-opensource-src (5.1.0-1) experimental; urgency=low

  * New upstream release.
  * Fix watch file.
  * Bump Qt  build dependencies to 5.1.0~.
  * Export QT_SELECT instead of depending on qt5-default.
  * Remove add_license_files.patch, applied upstream.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Thu, 11 Jul 2013 21:48:00 -0300

qttranslations-opensource-src (5.0.2-1) experimental; urgency=low

  [ Dmitry Shachnev ]
  * Initial release. (Closes: #697509)
  * debian/patches/add_license_files.patch: add (L)GPL license files
    (backported from upstream).

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 11 May 2013 15:53:22 -0300
